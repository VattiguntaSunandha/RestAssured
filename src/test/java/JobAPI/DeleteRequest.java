package JobAPI;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import RestAssuredUtlity.Xutility;
import io.restassured.response.Response;

public class DeleteRequest {

	@SuppressWarnings("unchecked")
	@Test(dataProvider="JOBSAPI")
	public void Deletejob(String JobId)
	{
	baseURI="https://jobs123.herokuapp.com";
	JSONObject requestdelete=new JSONObject();
	requestdelete.put("Job Id",JobId);
	
	Response Deleterequest=given().auth().basic("admin", "password")
			.contentType("application/json").body(requestdelete.toJSONString())
			.when().delete("/Jobs").then().log().all().extract().response();
	 int statuscode=Deleterequest.getStatusCode();
	 String responsebody=Deleterequest.getBody().asString();
	 
	  if (statuscode==200)
	  {
	  assertEquals(Deleterequest.contentType(),"application/json");
	  assertEquals(Deleterequest.getStatusLine(),"HTTP/1.1 200 OK");
	  assertEquals(responsebody.contains("JobId"),false);
	  System.out.println("Response:"+responsebody);
	  System.out.println("Job is deleted");
	  
	  
	 } else if (statuscode==404)
	 {
		 System.out.println("Job Doesn't Exists/Already Deleted");
		 assertEquals(Deleterequest.contentType(),"application/json");
		 assertEquals(Deleterequest.getStatusLine(),"HTTP/1.1 404 NOT FOUND");
		 System.out.println("Response:"+responsebody);
	 }
	  
     }
	
	@DataProvider(name = "JOBSAPI")
	public String[][] getjobs() throws IOException {
		System.out.println("Creating Job Using Excel");
		String path = "C:/Sunandha/Eclipse/RestAssuredAPITesting/src/test/resources/XlsData/JOBSAPI.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("DeleteJob");
		int totalcols = xlutil.getCellCount("DeleteJob", 1);
		//System.out.println(totalrows + "," + totalcols);
		String[][] JobDelete = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				JobDelete[i - 1][j] = xlutil.getCellData("DeleteJob", i, j);

			}
		}
		return JobDelete;
	}
	
}
