package JobAPI;

import static org.testng.Assert.assertEquals;

import java.io.File;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

import io.restassured.http.Method;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetJobs {
	
	@Test
	public void getAllJobs()
	{
		baseURI="https://jobs123.herokuapp.com";

		RequestSpecification getrequest=given();
		getrequest.auth().basic("admin", "password");
		Response responsegot=getrequest.request(Method.GET,"/Jobs");
		
		String responsebody=responsegot.getBody().asString();
		String reresponsebody=responsebody.replace("NaN", "Null");
		String response=reresponsebody.replace(",","} {");
		//String responseheader=responsegot.getHeader(response);
		int statuscode=responsegot.getStatusCode();
		String statusline=responsegot.getStatusLine();
		assertEquals(statuscode,200);
		assertEquals(response!=null,true);
		System.out.println("Reponse "+statuscode+" Successfull");
		System.out.println("Reponse Status Recieved is "+statusline);
	   // System.out.println("Response Body is "+response);
	    responsegot.then().assertThat().log().all();
	    
	    
	    given().when().then().body(JsonSchemaValidator. matchesJsonSchema(
	    		new File("src/test/resources/ValidateSchema/GetJobs.Json")));
	    System.out.println("Json Scheme Validated Successfully");
	    
	   	}

		
		/*
		 * @Test public void getSchemaValidate() {
		 * given().when().then().body(JsonSchemaValidator. matchesJsonSchema(new File(
		 * "C:/Sunandha/Eclipse/RestAssuredAPITesting/src/test/resources/ValidateSchema/GetJobs.Json"
		 * ))); }
		 */
		 
	
}
