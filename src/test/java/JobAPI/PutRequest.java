package JobAPI;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import RestAssuredUtlity.Xutility;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;

public class PutRequest {
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider="JOBSAPI")
	public void putjob(String JobId,String JobTitle)
	{
	baseURI="https://jobs123.herokuapp.com";
	JSONObject requestparam=new JSONObject();
	requestparam.put("Job Id",JobId);
	requestparam.put("Job Title",JobTitle);
	
	Response putrequest=given().auth().basic("admin", "password")
			.contentType("application/json").body(requestparam.toJSONString())
			.when().put("/Jobs").then().log().all().extract().response();
	 int statuscode=putrequest.statusCode();
	 String responsebody=putrequest.getBody().asString();
	  if (statuscode==200)
	  {
	  assertEquals(putrequest.contentType(),"application/json");
	  assertEquals(putrequest.getStatusLine(),"HTTP/1.1 200 OK");
	  assertEquals(responsebody.contains("198200"),true);
	  assertEquals(responsebody.contains("CEO"),true);
	  System.out.println("Response:"+responsebody);
	  
	 } else if (statuscode==404)
	 {
		 System.out.println("Job Doesn't Exists");
		 assertEquals(putrequest.contentType(),"application/json");
		 assertEquals(putrequest.getStatusLine(),"HTTP/1.1 404 NOT FOUND");
		 System.out.println("Response:"+responsebody);
	 }	  
	  
		given().when().then().body(JsonSchemaValidator.
		matchesJsonSchema(new File("src/test/resources/ValidateSchema/GetJobs.Json")));
		System.out.println("Schema Validation is Successfull");
}

	
	@DataProvider(name = "JOBSAPI")
	public String[][] getjobs() throws IOException {
		System.out.println("Creating Job Using Excel");
		String path = "C:/Sunandha/Eclipse/RestAssuredAPITesting/src/test/resources/XlsData/JOBSAPI.xlsx";
		Xutility xlutil = new Xutility(path);
		int totalrows = xlutil.getRowCount("PutJob");
		int totalcols = xlutil.getCellCount("PutJob", 1);
		//System.out.println(totalrows + "," + totalcols);
		String[][] JobsPut = new String[totalrows][totalcols];
		for (int i = 1; i <= totalrows; ++i) {
			for (int j = 0; j < totalcols; ++j) {
				JobsPut[i - 1][j] = xlutil.getCellData("PutJob", i, j);

			}
		}
		return JobsPut;
	}
	
	
	
	}

